# 个人主页导航


* 纯粹由 html+css 编写，无外部库依赖

* 代码简洁仅 300 多行，不含背景图，单 html 文件仅 8K 大小

* 悬停右侧彩色按钮，自动弹出分类导航

* 响应式设计，适配 PC 端和手机端


* [展示地址](https://chwt163.gitee.io/navigation/)

* 截图：

![snapshot](snapshot/snapshot.jpg)


![snapshot](snapshot/snapshot-2.jpg)


###################
# # 版权所有 # #
###################

